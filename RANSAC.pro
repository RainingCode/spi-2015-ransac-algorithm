#-------------------------------------------------
#
# Project created by QtCreator 2015-08-07T13:33:25
#
#-------------------------------------------------

QMAKE_CXXFLAGS += -Wno-deprecated


QT       += core gui
QT_CONFIG -= no-pkg-config
CONFIG += link_pkgconfig

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RANSAC
TEMPLATE = app


SOURCES += main.cpp\
        ransacui.cpp \

HEADERS  += ransacui.h \

FORMS    += ransacui.ui



#Path for PCL include files
INCLUDEPATH += "/usr/include/pcl-1.7/"

#Path for Eigen
INCLUDEPATH += "/usr/include/eigen3/"

#Path for VTK
INCLUDEPATH += "/usr/include/vtk-5.8"

#Path for Boost, FLANN, QHull
INCLUDEPATH += "/usr/include/"


#Path for all other Libs
LIBS += -L/usr/lib/
LIBS += -L/usr/local/lib/


#Path for Boost
LIBS += -lboost_system
LIBS += -lboost_thread

#Path for VTK Lib
LIBS+=-lpthread
LIBS+=-lm
#Path to PCL
LIBS += -lpcl_apps
LIBS += -lpcl_common
LIBS += -lpcl_filters
LIBS += -lpcl_keypoints
LIBS += -lpcl_kdtree
LIBS += -lpcl_search
LIBS += -lpcl_features
LIBS += -lpcl_io
LIBS += -lpcl_io_ply
LIBS += -lpcl_visualization

#RANSAC
LIBS += -lpcl_sample_consensus
LIBS += -lpcl_segmentation
LIBS += -lpcl_registration
LIBS += -lpcl_filters

#VTK
LIBS += -lvtkFiltering
