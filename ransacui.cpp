#include "ransacui.h"
#include "ui_ransacui.h"

RansacUI::RansacUI(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RansacUI)
{

    this->InitExecutionSteps();
    this->InitData();
    ui->setupUi(this);
    this->InitUI();
}

void RansacUI::InitExecutionSteps()
{

    ExecutionStep step;
    step.first = &RansacUI::LoadPointCloud;
    step.second = "Loading Cloud...";

    this->executionSteps.append(step);

    step.first = &RansacUI::CalculateNormals;
    step.second = "Calculating Normals...";

    this->executionSteps.append(step);
}

void RansacUI::InitData()
{

    this->DetectableShapes.insert("Circle 2D",              qMakePair(pcl::SACMODEL_CIRCLE2D,  QColor("cyan")));
    this->DetectableShapes.insert("Circle 3D",              qMakePair(pcl::SACMODEL_CIRCLE3D, QColor("darkCyan")));
    this->DetectableShapes.insert("Cone",                   qMakePair(pcl::SACMODEL_CONE, QColor("red")));
    this->DetectableShapes.insert("Cylinder",               qMakePair(pcl::SACMODEL_CYLINDER, QColor("darkRed")));
    this->DetectableShapes.insert("Line",                   qMakePair(pcl::SACMODEL_LINE, QColor("magenta")));
    this->DetectableShapes.insert("Normal Parallel Plane",  qMakePair(pcl::SACMODEL_NORMAL_PARALLEL_PLANE, QColor("darkMagenta")));
    this->DetectableShapes.insert("Normal Plane",           qMakePair(pcl::SACMODEL_NORMAL_PLANE, QColor("green")));
    this->DetectableShapes.insert("Normal Sphere",          qMakePair(pcl::SACMODEL_NORMAL_SPHERE, QColor("darkGreen")));
    this->DetectableShapes.insert("Parallel Line",          qMakePair(pcl::SACMODEL_PARALLEL_LINE, QColor("yellow")));
    this->DetectableShapes.insert("Parallel Lines",         qMakePair(pcl::SACMODEL_PARALLEL_LINES, QColor("darkYellow")));
    this->DetectableShapes.insert("Parallel Plane",         qMakePair(pcl::SACMODEL_PARALLEL_PLANE, QColor("blue")));
    this->DetectableShapes.insert("Perpendicular Plane",    qMakePair(pcl::SACMODEL_PERPENDICULAR_PLANE, QColor("darkBlue")));
    this->DetectableShapes.insert("Plane",                  qMakePair(pcl::SACMODEL_PLANE, QColor("lightGray")));
    this->DetectableShapes.insert("Sphere",                 qMakePair(pcl::SACMODEL_SPHERE, QColor("darkGray")));
    this->DetectableShapes.insert("Stick",                  qMakePair(pcl::SACMODEL_STICK, QColor(255, 64, 64)));
    this->DetectableShapes.insert("Torus",                  qMakePair(pcl::SACMODEL_TORUS, QColor(255, 127, 36)));

    this->SampleConsenusAlgorithms.insert("LMedS (Least Median of Squares)",                            pcl::SAC_LMEDS);
    this->SampleConsenusAlgorithms.insert("MLESAC( Maximum Likelihood Estimator SAmple Consensus)",     pcl::SAC_MLESAC);
    this->SampleConsenusAlgorithms.insert("MSAC (M-estimator SAmple Consensus)",                        pcl::SAC_MSAC);
    this->SampleConsenusAlgorithms.insert("PROSAC (Progressive Sample Consensus)",                      pcl::SAC_PROSAC);
    this->SampleConsenusAlgorithms.insert("RANSAC (RAndom SAmple Consensus)",                           pcl::SAC_RANSAC);
    this->SampleConsenusAlgorithms.insert("RMSAC (Randomized M-estimator SAmple Consensus)",             pcl::SAC_RMSAC);
    this->SampleConsenusAlgorithms.insert("RRANSAC (Randomized RAndom SAmple Consensus)",               pcl::SAC_RRANSAC);
}

void RansacUI::InitUI()
{
    QHash<QString, QPair<pcl::SacModel, QColor> >::iterator shapesIterator = this->DetectableShapes.begin();
    while(shapesIterator != this->DetectableShapes.end())
    {
        QListWidgetItem *item = new QListWidgetItem;
        item->setData( Qt::DisplayRole,  shapesIterator.key());
        item->setData( Qt::CheckStateRole, Qt::Checked );

        item->setForeground(shapesIterator.value().second);

        ui->DetectableShapesWidget->addItem(item);

        ++shapesIterator;
    }

    QHash<QString, int>::Iterator algorithmIterator = this->SampleConsenusAlgorithms.begin();
    while(algorithmIterator != this->SampleConsenusAlgorithms.end())
    {
        ui->AlgorithmComboBox->addItem(algorithmIterator.key());
        ++algorithmIterator;
    }
}


void RansacUI::LoadPointCloud()
{
    this->sourceCloud.reset(new PointCloudXYZRGB);
    pcl::PCDReader reader;
    if (reader.read(ui->FileTextBox->text().toStdString(), *this->sourceCloud)!= 0) //* load the file
    {
        qDebug() << "Couldn't read the pointCloud";
        this->error = true;
        this->errorText = "Could not read the PointCloud";
        return;
    }
    qDebug() << "Cloud size: " << this->sourceCloud->points.size();
}

void RansacUI::CalculateNormals()
{


    this->sourceCloudNormals.reset(new PointCloudNormal);
    if(!ui->NormalPointCloudTextBox->text().isEmpty())
    {
        pcl::PCDReader reader;
        if(reader.read(ui->NormalPointCloudTextBox->text().toStdString(), *this->sourceCloudNormals) != -1 && this->sourceCloudNormals->points.size() == this->sourceCloud->points.size())
        {
            qDebug() << "Loaded Normals from file...";
            return;
        }
    }


    pcl::NormalEstimationOMP<PointXYZRGB, PointNormal> ne;
    pcl::search::KdTree<PointXYZRGB>::Ptr tree(new pcl::search::KdTree<PointXYZRGB>());

    ne.setSearchMethod(tree);
    ne.setInputCloud(this->sourceCloud);
    ne.setRadiusSearch(ui->SearchRadiusSpinBox->value());
    ne.compute(*this->sourceCloudNormals);

    pcl::PCDWriter writer;
    writer.write(ui->FileTextBox->text().append(".normals.pcd").toStdString(), *this->sourceCloudNormals);
    ui->NormalPointCloudTextBox->setText(ui->FileTextBox->text().append(".normals.pcd"));
}

PointCloudXYZRGB::Ptr RansacUI::DetectShape(pcl::SacModel model, QString modelName, QColor outputColor, int algorithm)
{
    PointCloudXYZRGB::Ptr resultCloud(new PointCloudXYZRGB());
    if(this->sourceCloud->points.empty())
    {
        return resultCloud;
    }

    bool requiresNormals = this->AreNormalsRequired();

    // Temporary Clouds
    PointCloudNormal::Ptr remainingNormals(new PointCloudNormal);
    PointCloudXYZRGB::Ptr remainingCloud(new PointCloudXYZRGB);


    // fill temporary cloud
    pcl::copyPointCloud(*this->sourceCloud, *remainingCloud);

    if(requiresNormals)
    {
        pcl::copyPointCloud(*this->sourceCloudNormals, *remainingNormals);
    }

    pcl::SACSegmentation<PointXYZRGB> *seg;

    if(requiresNormals)
    {
        seg = new pcl::SACSegmentationFromNormals<PointXYZRGB, PointNormal>();
        ((pcl::SACSegmentationFromNormals<PointXYZRGB, PointNormal> *)seg)->setInputNormals(remainingNormals);
        ((pcl::SACSegmentationFromNormals<PointXYZRGB, PointNormal> *)seg)->setNormalDistanceWeight(ui->NormalWeightDistanceDoubleSpinBox->value());
        ((pcl::SACSegmentationFromNormals<PointXYZRGB, PointNormal> *)seg)->setMinMaxOpeningAngle(ui->MinOpeningAngleSpinBox->value(), ui->MaxOpeningAngleSpinBox->value());
    }else
    {
        seg = new pcl::SACSegmentation<PointXYZRGB>();
    }

    // Init algorithm
    seg->setOptimizeCoefficients(this->ui->OptimizeCoefficientsCheckBox->checkState());
    seg->setMethodType (algorithm);
    seg->setModelType(model);
    seg->setMaxIterations (ui->IterationsSpinBox->value());
    seg->setDistanceThreshold (ui->ThresholdDistanceSpinBox->value());
    seg->setProbability(ui->ProbabilitySpinBox->value());
    seg->setRadiusLimits(ui->MiniumRadiusSpinBox->value(), ui->MaximumRadiusSpinBox->value());

    // cloud for the segmentation
    PointCloudXYZRGB::Ptr segmented(new PointCloudXYZRGB);
    do{

        pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
        pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

        // Set the clouds
        seg->setInputCloud (remainingCloud);
        if(requiresNormals){

            ((pcl::SACSegmentationFromNormals<PointXYZRGB, PointNormal> *)seg)->setInputNormals (remainingNormals);
        }

        // Obtain the inliers and coefficients
        seg->segment (*inliers, *coefficients);

        // If nothing there, we are finished
        if(inliers->indices.size() == 0)
        {
            return resultCloud;
        }


        // Extract the inliers from the input cloud
        pcl::ExtractIndices<PointXYZRGB> extract;
        extract.setInputCloud (remainingCloud);
        extract.setIndices (inliers);
        extract.setNegative (false);

        segmented.reset(new PointCloudXYZRGB);
        extract.filter (*segmented);


        // Remove noise
        std::vector<int> indices;
        pcl::removeNaNFromPointCloud(*segmented, *segmented, indices);

        // Filter

        // euclidean clustering
        typename pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
        tree->setInputCloud (segmented);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> clustering;
        clustering.setClusterTolerance (0.02); // 2cm
        clustering.setMinClusterSize (1000);
        clustering.setMaxClusterSize (250000);
        clustering.setSearchMethod (tree);
        clustering.setInputCloud(segmented);
        clustering.extract (cluster_indices);

        if (cluster_indices.size() > 0)//use largest cluster
        {
            qDebug() << cluster_indices.size() << " clusters found";
            if (cluster_indices.size() > 1)
                qDebug() <<" Using largest one...";

            typename pcl::IndicesPtr indices (new std::vector<int>);
            *indices = cluster_indices[0].indices;
            extract.setInputCloud (segmented);
            extract.setIndices (indices);
            extract.setNegative (false);

            extract.filter (*segmented);

            // remove the data
            extract.setInputCloud(remainingCloud);
            extract.setIndices(indices);
            extract.setNegative(true);
            extract.filter(*remainingCloud);


            if(requiresNormals){
                pcl::ExtractIndices<PointNormal> extract_normals;
                // Remove the Normals from the detected Shape for next detection step
                extract_normals.setInputCloud(remainingNormals);
                extract_normals.setIndices(inliers);
                extract_normals.setNegative(true);
                extract_normals.filter(*remainingNormals);
            }

            // Copy it to the result
            for (size_t i = 0; i < segmented->points.size(); i++)
            {

                segmented->points[i].r = outputColor.red();
                segmented->points[i].g = outputColor.green();
                segmented->points[i].b = outputColor.blue();
                resultCloud->points.push_back(segmented->points[i]);
            }
        }else
        {
            // remove the data
            extract.setInputCloud(remainingCloud);
            extract.setIndices(inliers);
            extract.setNegative(true);
            extract.filter(*remainingCloud);

            if(requiresNormals){
                pcl::ExtractIndices<PointNormal> extract_normals;
                // Remove the Normals from the detected Shape for next detection step
                extract_normals.setInputCloud(remainingNormals);
                extract_normals.setIndices(inliers);
                extract_normals.setNegative(true);
                extract_normals.filter(*remainingNormals);
            }

        }

    }  while(!remainingCloud->points.empty() && !segmented->points.empty());



    /*
        if(algorithmResultCloud->points.size() >= this->sourceCloud->size())
        {
            qDebug() << "Size greater than source";
        }


        qDebug() << "Found Shape " << modelName << ". Size: " <<  algorithmResultCloud->points.size() << " points";

        // Copy it to the result
        for (size_t i = 0; i < algorithmResultCloud->points.size(); i++)
        {

            algorithmResultCloud->points[i].r = outputColor.red();
            algorithmResultCloud->points[i].g = outputColor.green();
            algorithmResultCloud->points[i].b = outputColor.blue();
            this->resultCloud->points.push_back(algorithmResultCloud->points[i]);
        }

        // Remove the detected Shape from next detection step
        PointCloudXYZRGB::Ptr segmented(new PointCloudXYZRGB);
        extract.setInputCloud(remainingCloud);
        extract.setIndices(inliers);
        extract.setNegative(true);
        extract.filter(*segmented);

        // Remove noise
        std::vector<int> indices;
        pcl::removeNaNFromPointCloud(*segmented, *segmented, indices);

        typename pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<PointXYZRGB>);
        tree->setInputCloud (segmented);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<PointXYZRGB> clustering;
        clustering.setClusterTolerance (0.02); // 2cm
        clustering.setMinClusterSize (1000);
        clustering.setMaxClusterSize (250000);
        clustering.setSearchMethod (tree);
        clustering.setInputCloud(segmented);
        clustering.extract (cluster_indices);

        if (cluster_indices.size() > 0)//use largest cluster
        {
          qDebug() << cluster_indices.size() << " clusters found";
          if (cluster_indices.size() > 1)
            qDebug()  <<" Using largest one...";

          typename pcl::IndicesPtr indices (new std::vector<int>);
          *indices = cluster_indices[0].indices;
          extract.setInputCloud (segmented);
          extract.setIndices (indices);
          extract.setNegative (false);

          extract.filter (*segmented);
          pcl::copyPointCloud(*segmented, *this->resultCloud);
        }

        pcl::copyPointCloud(*segmented, *remainingCloud);

        segmented.reset();

        // Remove the Normals from the detected Shape for next detection step
        PointCloudNormal::Ptr tmpNormal(new PointCloudNormal);
        extract_normals.setInputCloud(remainingNormals);
        extract_normals.setIndices(inliers);
        extract_normals.setNegative(true);
        extract_normals.filter(*tmpNormal);

        pcl::copyPointCloud(*tmpNormal, *remainingNormals);

        tmpNormal.reset();
*/


}

void RansacUI::RunRANSAC()
{
    QProgressDialog progressDialog("Detecting Shapes.Please wait...", QString(), 0, 0,  this);
    progressDialog.setWindowTitle("Shape detection in progress");

    progressDialog.setWindowFlags(Qt::Dialog  | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

    progressDialog.setWindowModality(Qt::WindowModal);

    progressDialog.show();
    QApplication::processEvents();
    this->error = false;
    this->errorText = "";

    bool requiresNormals = this->AreNormalsRequired();

    for(int i = 0; i < this->executionSteps.count(); i++)
    {
        ExecutionStep currentStep = this->executionSteps[i];

        if(currentStep.second.contains("normals", Qt::CaseInsensitive)  && !requiresNormals)
        {
            // Skip... No normals requiered
            continue;
        }

        progressDialog.setLabelText(currentStep.second);

        QFuture<void> future = QtConcurrent::run(this, currentStep.first);

        while (!future.isFinished())
        {
            usleep(500 * 1000);
            QApplication::processEvents();
        }

        if(this->error)
        {
            progressDialog.hide();
            QApplication::processEvents();

            qDebug() << this->errorText;
            return;
        }
    }


    int algorithm = this->SampleConsenusAlgorithms[ui->AlgorithmComboBox->currentText()];

    for(int i = 0; i < ui->DetectableShapesWidget->count(); i++)
    {
        if(ui->DetectableShapesWidget->item(i)->checkState() != Qt::Checked)
        {
            continue;
        }
        progressDialog.setLabelText("Detecting " + ui->DetectableShapesWidget->item(i)->text() + "...");
        QPair<pcl::SacModel, QColor> modelParams= this->DetectableShapes[ui->DetectableShapesWidget->item(i)->text()];
        QFuture<PointCloudXYZRGB::Ptr> future = QtConcurrent::run(this, &RansacUI::DetectShape, modelParams.first, ui->DetectableShapesWidget->item(i)->text(), modelParams.second, algorithm);


        while (!future.isFinished())
        {
            usleep(500 * 1000);
            QApplication::processEvents();
        }


        if(future.result()->points.empty())
        {
            qDebug() << "No points found for" << ui->DetectableShapesWidget->item(i)->text();
            continue;
        }

        progressDialog.setLabelText("Saving result of "+ ui->DetectableShapesWidget->item(i)->text() + "...");
        future.result()->width = 1;
        future.result()->height = future.result()->points.size();
        pcl::io::savePCDFileASCII(ui->FileTextBox->text().append(".detected " + ui ->DetectableShapesWidget->item(i)->text() + ".pcd").toStdString(), *future.result());
    }

    progressDialog.hide();
    QApplication::processEvents();
}

QString RansacUI::ShowFileSelection()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open PointCloud"), "/home/vm/Schreibtisch/", tr("Point Cloud (*.pcd)"));


    return fileName;
}

void RansacUI::RefreshUiEnableStates()
{
    // Check if Filename is Empty
    bool isEnabled = !ui->FileTextBox->text().isNull() &&  !ui->FileTextBox->text().isEmpty();
    if(!isEnabled)
    {
        ui->RunRansacButton->setEnabled(isEnabled);
    }

    // Disable them intial. Only reenable them if needed
    ui->MinOpeningAngleSpinBox->setEnabled(false);
    ui->MaxOpeningAngleSpinBox->setEnabled(false);

    bool requiresNormals = this->AreNormalsRequired();
    ui->NormalEstimationParams->setEnabled(requiresNormals);
    ui->NormalWeightDistanceDoubleSpinBox->setEnabled(requiresNormals);
    ui->SelectNormalsButton->setEnabled(requiresNormals);
    ui->NormalPointCloudTextBox->setEnabled(requiresNormals);

    isEnabled = false;
    //Check if Shape is Selected
    for(int i = 0; i < ui->DetectableShapesWidget->count(); i++)
    {
        if(ui->DetectableShapesWidget->item(i)->checkState()  == Qt::Checked)
        {
            isEnabled = true;

            QPair<pcl::SacModel, QColor> model = this->DetectableShapes[ui->DetectableShapesWidget->item(i)->text()];

            if(model.first == pcl::SACMODEL_CONE)
            {
                ui->MinOpeningAngleSpinBox->setEnabled(true);
                ui->MaxOpeningAngleSpinBox->setEnabled(true);
                break;
            }
        }
    }

    ui->RunRansacButton->setEnabled(isEnabled);


}


RansacUI::~RansacUI()
{
    delete ui;
}

void RansacUI::on_SelectNormalsButton_clicked()
{
    QString fileName = this->ShowFileSelection();


    if(fileName.isNull() || fileName.isEmpty())
    {
        return;
    }
    ui->NormalPointCloudTextBox->setText(fileName);
}

void RansacUI::on_DetectableShapesWidget_clicked(const QModelIndex &index)
{
    this->RefreshUiEnableStates();
}

void RansacUI::on_DetectableShapesWidget_itemClicked(QListWidgetItem *item)
{
    this->RefreshUiEnableStates();
}

void RansacUI::on_RunRansacButton_released()
{
    this->RunRANSAC();
}

void RansacUI::on_FileTextBox_textChanged(const QString &arg1)
{
    this->RefreshUiEnableStates();
}

void RansacUI::on_FileSelectionButton_released()
{
    QString fileName = this->ShowFileSelection();

    if(fileName.isNull() || fileName.isEmpty())
    {
        return;
    }

    ui->FileTextBox->setText(fileName);

    QFileInfo normalFileInfo(ui->FileTextBox->text().append(".normals.pcd"));
    if(normalFileInfo.exists())
    {
        ui->NormalPointCloudTextBox->setText(normalFileInfo.absoluteFilePath());
    }
}

bool RansacUI::AreNormalsRequired()
{
    for(int i = 0; i < ui->DetectableShapesWidget->count(); i++)
    {
        if(ui->DetectableShapesWidget->item(i)->checkState()  == Qt::Checked)
        {

            QPair<pcl::SacModel, QColor> model = this->DetectableShapes[ui->DetectableShapesWidget->item(i)->text()];
            if(model.first == pcl::SACMODEL_CYLINDER || model.first == pcl::SACMODEL_NORMAL_PLANE ||
                    model.first == pcl::SACMODEL_NORMAL_PARALLEL_PLANE || model.first == pcl::SACMODEL_CONE ||
                    model.first == pcl::SACMODEL_NORMAL_SPHERE)
            {
                return true;
            }
        }
    }
    return false;
}

void RansacUI::on_DetectableShapesWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    this->RefreshUiEnableStates();
}
