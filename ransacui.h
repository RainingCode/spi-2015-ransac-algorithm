#ifndef RANSACUI_H
#define RANSACUI_H

#include <Qt>

#include <QDialog>
#include <QList>
#include <QFileDialog>
#include <QFileInfo>
#include <QString>
#include <QListWidget>
#include <QHash>
#include <QListWidgetItem>
#include <QDebug>
#include <QtConcurrentRun>
#include <QApplication>
#include <QProgressDialog>
#include <QPair>


// PCL Library Stuff (for loading the PCD File)
#include <pcl/io/pcd_io.h>

// Point Cloud
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


// Normal Estimation
#include <pcl/features/normal_3d_omp.h>

// Segmentation
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/sample_consensus/boost.h>
#include <pcl/sample_consensus/eigen.h>

// Clustering
#include <pcl/segmentation/extract_clusters.h>


// Boost
#include <boost/thread/thread.hpp>


class RansacUI;

typedef void (RansacUI::*functionPointer)(void);
typedef QPair<functionPointer, QString> ExecutionStep;


typedef pcl::PointXYZRGB PointXYZRGB;
typedef pcl::PointCloud<PointXYZRGB> PointCloudXYZRGB;

typedef pcl::PointNormal PointNormal;
typedef pcl::PointCloud<PointNormal> PointCloudNormal;

namespace Ui {


class RansacUI;
}
/**
 * @brief RansacUI This class contains the UI Logic and the Logic for the Shape Detection
 * @author Sebastian S. Schüler
 * @version 08.10.2015
 */
class RansacUI : public QDialog
{
    Q_OBJECT



public:
    /**
     * @brief RansacUI Constructor to create an user interface for the shape detection
     * @param parent the parent of the class
     */
    explicit RansacUI(QWidget *parent = 0);
    ~RansacUI();

private:
    Ui::RansacUI *ui;
    /**
     * @brief RansacShapes This QHash contains all Shapes which are Detectable
     */
    QHash<QString, QPair<pcl::SacModel, QColor> > DetectableShapes;

    /**
     * @brief SampleConsenusAlgorithms This QHash contains all Algorithms which can be executed
     */
    QHash<QString, int> SampleConsenusAlgorithms;



    /**
     * @brief error an flag if an error occured on an execution step
     */
    bool error;

    /**
     * @brief errorText if the Error Flag is set, then this string will contain the Error description
     */
    QString errorText;

    /**
     * @brief executionSteps A List of Steps which needs to be executed before the algorithm can run (Load cloud, calculate normals, etc.)
     */
    QList<ExecutionStep> executionSteps;

    /**
     * @brief sourceCloud the loaded cloud
     */
    PointCloudXYZRGB::Ptr sourceCloud;
    /**
     * @brief sourceCloudNormals the normals for the cloud
     */
    PointCloudNormal::Ptr sourceCloudNormals;

    /**
     * @brief InitExecutionSteps This Method will Initialize all Execcution Steps which are requiered before the Algorithm can run (normal detection, etc.)
     */
    void InitExecutionSteps();
    /**
     * @brief InitData This Method will fill the RansacShapes and the Algorithm QHash
     */
    void InitData();
    /**
     * @brief InitUI This Method will fill the UI (Combobox for algorithm and Shapes List) based on the Qhashes
     */
    void InitUI();

    /**
     * @brief RefreshUiEnableStates This Method will check the condition to Enable/Disable for UI Components
     */
    void RefreshUiEnableStates();

    /**
     * @brief LoadPointCloud This Method will load the PointCloud
     */
    void LoadPointCloud();
    /**
     * @brief CalculateNormals This Method will Calculate the PointCloud for the Source Cloud
     */
    void CalculateNormals();


    /**
     * @brief DetectShape This Method will execute the Logic of the SAC Algorithm and adds the result to the result Cloud
     * @param model The model to detect
     * @param modelName the name of the model
     * @param outputColor the color of the model
     * @param algorithm the algorithm to use (see methodTypes.h from PCL)
     * @return Returns the result of the detection
     */
   PointCloudXYZRGB::Ptr DetectShape(pcl::SacModel model,QString modelName, QColor outputColor, int algorithm);



    /**
     * @brief RunRANSAC This method will run everything (from loading the cloud to running the SAC Algorithm)
     */
    void RunRANSAC();

    /**
     * @brief ShowFileSelection This method will show an File Selection Dialog
     * @return returns the filename
     */
    QString ShowFileSelection();

    /**
     * @brief AreRequiresNormals This Method will return if models are requiered for the Actual Selection
     * @return if they are requiered or not
     */
    bool AreNormalsRequired();
private slots:


    void on_SelectNormalsButton_clicked();
    void on_DetectableShapesWidget_itemClicked(QListWidgetItem *item);
    void on_DetectableShapesWidget_clicked(const QModelIndex &index);
    void on_RunRansacButton_released();
    void on_FileTextBox_textChanged(const QString &arg1);
    void on_FileSelectionButton_released();
    void on_DetectableShapesWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
};

#endif // RANSACUI_H
